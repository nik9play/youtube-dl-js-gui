//setting up modules
var fs = require('fs');
var youtubedl = require('youtube-dl');
var $ = require("jquery");
var {remote,shell} = require('electron')
var mainProcess = remote.require(__dirname + '/index.js')
var {dialog} = require('electron').remote
var M = require('materialize-css')


//init download wo queue modal
var dwom = M.Modal.init($("#dwom")[0], [{dismissible: false}]);
//init materilizecss
M.AutoInit();
//init queue array
var queue = []
//remove item from queue function
function remove_item(id) {
    $("#" + id).remove()
    queue = queue.filter( el => el.id !== id );
};
//sort qualities to select function
function mapInfo(item) {
    'use strict';
    console.log(item)
    return {
        itag: item.format_id,
        filetype: item.ext,
        resolution: item.resolution || ((item.width) ? item.width + 'x' + item.height : 'Звук'),
        bitrate: ((item.tbr) ? item.tbr : item.abr)
    };
}
//search in queue function 
function search(nameKey, myArray){
    for (var i=0; i < myArray.length; i++) {
        if (myArray[i].id === nameKey) {
            return i;
        }
    }
}
//change quality function
function changeQuality(select) {
    var val = select.value;
    var parent = $(select).parents()[4]
    var id = $(parent).attr("id")
    var index = search(id, queue)
    queue[index].quality = val
    M.toast({html: 'Сохранено'})
}

function download_wo_queue(id, title) {
    $("#dwom_title").html(title)
    $("#dwom_btn").click(function() {

        var path = $("#dwom_input_dir").val()
        path = path.replace(/([\\])/g, "/") + "/"
        if ($("#dwom_input_dir").val() == "") {
            $("#dwom_input_dir").addClass("invalid")
            setTimeout(() => {
                $("#dwom_input_dir").removeClass("invalid")
            }, 3000);
        } else {
            $("#dwom_dir").hide();
            $("#dwom_loading").show();
    
            var quality = $("#" + id + "_selector").val()

            youtubedl.exec(id, ['-f', quality, '-o', path + '%(title)s', '--ffmpeg-location', 'exec/' + process.platform + '/ffmpeg' + ((process.platform == "win32") ? ".exe" : ""), '--prefer-ffmpeg'], {}, function exec(err, output) {
                'use strict';
                if (err) { 
                    dwom.close()
                    $("#dwom_dir").show();
                    $("#dwom_loading").hide();
                    M.toast({html: 'Произошла ошибка! Смотрите консоль для подробностей'}); 
                    throw err; 
                } else {
                    dwom.close()
                    $("#dwom_dir").show();
                    $("#dwom_loading").hide();
                    M.toast({html: 'Скачано в ' + path})
                }
            }); 
        }
    })
    dwom.open()
}

function add_video_to_queue(url) {
    var options = [];

    $("#add_btn").prop("disabled", true);
    $("#add_input").prop("disabled", true);

    $("#progress_container").prepend('<div class="progress z-depth-1" style="margin: 0;background: #e3f2fd;"><div class="indeterminate" style="background-color: #2196f3;"></div></div>')

    youtubedl.getInfo(url, options, function(err, info) {

        if (err) {
            $(".progress").remove();
            $("#add_btn").prop("disabled", false);
            $("#add_input").prop("disabled", false);
            M.toast({html: 'Произошла ошибка! Смотрите консоль для подробностей'})
            throw err;
        } else {
            console.log('id:', info.id);
            console.log('title:', info.title);
            console.log('url:', info.url);
            console.log('thumbnail:', info.thumbnail);
            console.log('description:', info.description);
            console.log('filename:', info._filename);
            console.log('format id:', info.format_id);
            console.log('format:', info);

            var formats_array = info.formats.map(mapInfo)

            formats_array.sort(function(a, b) {
                return parseFloat(b.bitrate) - parseFloat(a.bitrate);
            });

            var formats = "<option value='bestvideo[ext=mp4]+bestaudio[ext=m4a]'>Лучшее качество</option>";
            formats_array.forEach(el => {
                formats = formats + "<option value='" + el.itag + "'>" + el.resolution + " (" + el.filetype + ") – " + ((typeof el.bitrate == "undefined") ? "Не определен" : el.bitrate + " kbps") + "</option>\n"
            })

            queue.push({url: url, id: info.id, filename: info._filename, quality: "bestvideo"});

            $("#cards").prepend(`
            <div class="col s12 m6 xl4" id="${info.id}">
                <div class="card">
                    <div class="card-image">
                        <img src="${info.thumbnail}">
                        <span class="card-title">${info.title}</span>
                        <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons" onclick="remove_item('${info.id}')">close</i></a>
                    </div>
                    <div class="card-content">
                        <p><a href="" onclick="shell.openExternal('${info.uploader_url}');">${info.uploader}</a></p>
                    </div>
                    <div class="card-action">
                        <div class="input-field">
                            <select onchange="changeQuality(this)" id="${info.id + '_selector'}">
                                ${formats}
                            </select>
                            <label>Качество</label>
                        </div>
                        <a class="waves-effect waves-light btn blue darken-1" style="width:100%" onclick="download_wo_queue('${info.id}', '${info.title}')">Скачать без очереди</a>
                    </div>
                </div>
            </div>
            `)
            M.AutoInit()

            $(".progress").remove();
            $("#add_btn").prop("disabled", false);
            $("#add_input").prop("disabled", false);
        }
    });

}

$(document).ready(function(){
    $("#add_btn").click(function() {
        if ($("#add_input").val() === "") {
            $("#add_input").addClass("invalid")
            setTimeout(() => {
                $("#add_input").removeClass("invalid")
            }, 3000);
        } else {
            add_video_to_queue(`${$('#add_input').val()}`)
            $('#add_input').val('')
            $("label[for='add_input']").removeClass("active");
        }
    })
    $("#dwom_btn_dir").click(function() {
        $("label[for='dwom_input_dir']").addClass("active");
        $("#dwom_input_dir").val(dialog.showOpenDialog({properties: ['openDirectory']}));
    })
});