const {app, remote, BrowserWindow} = require('electron')

let win

function createWindow () {
  win = new BrowserWindow({
    width: 1100, 
    height: 600,
    minWidth: 1100,
    minHeight: 600
  })

  win.loadFile('index.html')

  //win.webContents.openDevTools()

  win.on('closed', () => {
    win = null
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (win === null) {
    createWindow()
  }
})